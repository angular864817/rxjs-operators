import { Component } from '@angular/core';
import { from, interval } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { of, distinctUntilChanged } from 'rxjs';
import { forkJoin } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { mergeMap, map } from 'rxjs/operators';
import { timer, combineLatest } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'http_client';



  // throttleTime operator

// // emit value every 1 second
// source = interval(1000);
// /*
//   emit the first value, then ignore for 5 seconds. repeat...
// */
// example = this.source.pipe(throttleTime(5000));
// // output: 0...6...12
// subscribe = this.example.subscribe(val => console.log(val));



// distinctUntilChanged
// only output distinct values, based on the last emitted value
totallyDifferentBuilds$ = of(
  { engineVersion: '1.1.0', transmissionVersion: '1.2.0' },
  { engineVersion: '1.1.0', transmissionVersion: '1.4.0' },
  { engineVersion: '1.3.0', transmissionVersion: '1.4.0' },
  { engineVersion: '1.3.0', transmissionVersion: '1.5.0' },
  { engineVersion: '2.0.0', transmissionVersion: '1.5.0' }
).pipe(
  distinctUntilChanged((prev, curr) => {
    return (
      prev.engineVersion === curr.engineVersion ||
      prev.transmissionVersion === curr.transmissionVersion
    );
  })
);

out =  this.totallyDifferentBuilds$.subscribe(console.log);



// forkjoin
//  it waits for all observables to be completed then it only emits the last value from each observable Remember if any of the observable does not complete then the forkJoin() will never complete.
observable = forkJoin({
  foo: of(1, 2, 3, 4),
  bar: Promise.resolve(8),
  baz: timer(4000)
});
oo = this.observable.subscribe({
 next: value => console.log(value),
 complete: () => console.log('This is how it ends!'),
});



// pluck operator
// We can pluck multiple property like if we use an observable of array type then we can select multiple properties to be plucked.



source = from([
  { name: 'Joe',
   age: 30,
   job: {
    title: 'Developer',
     language: 'JavaScript'
    }
  },
  //will return undefined when no job is found
  { name: 'Sarah',
   age: 35 }
]);

//grab title property under job
example = this.source.pipe(pluck('job', 'title'));

//output: "Developer" , undefined
subscribe = this.example.subscribe(val => console.log(val));


//  mergeMap operator
 letters = of('a', 'b', 'c');
 result = this.letters.pipe(
  mergeMap(x => interval(1000).pipe(map(i => x+i))),
);
pp = this.result.subscribe(x => console.log(x));


// combine latest operator

// Combinelatest operator is used to execute multiple observable at once , it only emits the latest value from each source observable (does not like forkJoin() operator).

// timerOne emits first value at 1s, then once every 4s
timerOne$ = timer(1000, 4000);
// timerTwo emits first value at 2s, then once every 4s
timerTwo$ = timer(2000, 4000);
// timerThree emits first value at 3s, then once every 4s
timerThree$ = timer(3000, 4000);

// when one timer emits, emit the latest values from each timer as an array
rr = combineLatest(this.timerOne$, this.timerTwo$,this.timerThree$).subscribe(
  ([timerValOne, timerValTwo, timerValThree]) => {
    /*
      Example:
    timerThree first tick: 'Timer One Latest: 0, Timer Two Latest: 0, Timer Three Latest: 0
    timerOne second tick: 'Timer One Latest: 1, Timer Two Latest: 0, Timer Three Latest: 0
    timerTwo second tick: 'Timer One Latest: 1, Timer Two Latest: 1, Timer Three Latest: 0
  */
    console.log(
      `Timer One Latest: ${timerValOne},
     Timer Two Latest: ${timerValTwo},
     Timer Three Latest: ${timerValThree}`
    );
  }
);

}
